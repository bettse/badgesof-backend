exports = function (changeEvent) {
  const id = changeEvent.documentKey._id;
  const { updateDescription } = changeEvent;

  const path = `badgesOf/request/${id}`;
  const url = `https://sockethook.ericbetts.dev/hook/${path}`;

  if (updateDescription) {
    const { updatedFields } = updateDescription; // A document containing updated fields
    if (updatedFields) {
      const { status } = updatedFields;
      console.log(JSON.stringify(updateDescription));
      const response = context.http.post({
        url,
        body: { status },
        encodeBodyAsJSON: true,
      });
    }
  }
};
