const fs = require("fs");
const { execSync } = require("child_process");
const sharp = require("sharp");

const device = execSync(`readlink -f /dev/evolis`).toString().replace("\n", "");

// Constants
const badgePx = {
  height: 1016,
  width: 648,
};
const overlay = {
  /** Typical mag-stripe **/
  magstrip: [
    [0, 0, 439, 1016],
    [588, 0, 648, 1016],
  ],
};
const PREFIX = 0x1b; //ESCAPE
const SEPARATOR = ";";
const SUFFIX = 0x0d; // CARRIAGE RETURN
const NOT_FOUND = -1;
const NUL = 0x00;

// CONFIGS
const precision = 256;
const debug = true;

async function sleep(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
}

// echo "\x1bRr\x0d" > /dev/usb/lp0 && od -a --read-bytes=10 /dev/usb/lp0
async function main() {
  try {
    const [_node, _script, ...commands] = process.argv;
    const results = await sendAndReceive(commands);
    results.forEach((x) => console.log(x));
    //await print("mclovin.png");
  } catch (e) {
    console.log("main error:", e);
  }
}

async function sendAndReceive(cmds) {
  console.log({ cmds });
  const results = [];

  for (var i = 0; i < cmds.length; i++) {
    if (debug) {
      console.log("send", cmds[i]);
    }
    await sendCommand(cmds[i]);
    results[i] = await retryRead();
    if (debug) {
      console.log("receive", results[i]);
    }
  }
  return results;
}

// sharp, the node library, can convert to cymk, but doesn't seem to do a great job
function rgb2cymk(buffer) {
  const channels = 3;
  const length = buffer.length / channels;
  const [r, g, b] = Array.from(buffer).reduce(
    (rgb, x, i) => {
      rgb[i % channels].push(x);
      return rgb;
    },
    [[], [], []]
  );

  const c = Buffer.alloc(length).fill(0);
  const m = Buffer.alloc(length).fill(0);
  const y = Buffer.alloc(length).fill(0);
  const k = Buffer.alloc(length).fill(0);

  // Method used qz: https://github.com/qzind/tray/pull/314/files#diff-0c27d82f7b29e8caddb47c6a215448e59c2cd437163beaf81b212df4510566b4R580-R586
  for (var i = 0; i < length; i++) {
    if (r[i] === 0 && g[i] === 0 && b[i] === 0) {
      k[i] = 0xff;
    } else {
      c[i] = 0xff - r[i];
      m[i] = 0xff - g[i];
      y[i] = 0xff - b[i];
    }
  }

  return { c, y, m, k };
}

function validatePanelSize({ c, m, y }, precision) {
  const bits = Math.log2(precision);
  const target = (badgePx.width * badgePx.height * bits) / 8; // 576072; // Number of bytes if you store 7 bit color values all packed together;
  const cLen = compact(c, precision).length;
  const mLen = compact(m, precision).length;
  const yLen = compact(y, precision).length;
  if (cLen != target || mLen != target || yLen != target) {
    console.log(
      {
        cLen,
        mLen,
        yLen,
      },
      "vs",
      target
    );
    throw "One of the channels had an incorrect size";
  }
}

async function print(filename) {
  const before = [
    "Pem;2", // To set software printer error recovery, software checks all answer from printer after sending an escape command
    //"Pem;0", // To set automatic printer error recovery, printer busy line is set to high level on error.
    // qz tray wiki
    //"Pps;0", // Enable raw/disable driver printer parameter supervision
    //"Wcb;k;0", // Clear card memory
    "Ss", // Start of sequence
    "Sr", //set recto(front) card size
  ];
  const after = [
    "Se", // End of sequence
    "Pem;0", // Error recovery are now managed by the printer.
  ];

  const { width, height } = await sharp(filename).metadata();
  const rotation = width > height ? 90 : 0;

  const buffer = await sharp(filename)
    .removeAlpha()
    .rotate(rotation)
    .resize(badgePx)
    .raw()
    .toBuffer();

  const { c, m, y, k } = rgb2cymk(buffer);

  // TODO: exclude things like magstripe or contact points from overlay
  const o = Buffer.alloc((badgePx.width * badgePx.height) / 8).fill(0xff);

  validatePanelSize({ c, m, y }, precision);

  /*
  for (var i = 0; i < badgePx.height; i++) {
    const start = i * badgePx.width;
    console.log(k.slice(start, start + 100).toString("hex"));
  }
  */

  const yPanel = Buffer.concat([
    Buffer.from(`Db;y;${precision};`),
    compact(y, precision),
  ]);
  const mPanel = Buffer.concat([
    Buffer.from(`Db;m;${precision};`),
    compact(m, precision),
  ]);
  const cPanel = Buffer.concat([
    Buffer.from(`Db;c;${precision};`),
    compact(c, precision),
  ]);
  const kPanel = Buffer.concat([Buffer.from("Db;k;2;"), compact(k, 2)]);
  const oPanel = Buffer.concat([Buffer.from("Db;o;2;"), o]);

  try {
    await sendAndReceive(before, true);
    // YMCKO
    await sendAndReceive([yPanel], true);
    await sendAndReceive([mPanel], true);
    await sendAndReceive([cPanel], true);
    await sendAndReceive([kPanel], true);
    await sendAndReceive([oPanel], true);
    await sendAndReceive(after, true);
  } catch (e) {
    console.log("error printing", e);
  }
}

// For debugging compact
function printBinary(buffer) {
  const a = Array.from(buffer);
  const parts = a.map((b) => b.toString(2).padStart(8, "0"));
  return parts.join("");
}

// Not to be confused with the compression used by `Dbc`
function compact(colorData, precision = 128) {
  const octet = 8;
  const msb = 7; // highest bit index in a byte
  const bits = Math.log2(precision);
  if (bits === octet) {
    // 8 bit to 8 bit => no-op
    return colorData;
  }
  const mask = precision - 1;
  const outputLength = Math.ceil((colorData.length * bits) / octet);
  const output = Buffer.alloc(outputLength).fill(0);
  colorData.forEach((x, i) => {
    // calculate less precise color
    const lossy = (x >> (octet - bits)) & mask;
    // What index in the new array
    const targetIndex = Math.floor((i * bits) / octet);
    // Index within byte
    const startPosition = msb - ((bits * i) % octet);
    const overflow = msb - startPosition + bits - octet;
    if (overflow <= 0) {
      output[targetIndex] |= lossy << (startPosition - (bits - 1));
    } else {
      output[targetIndex] |= lossy >> (bits - (startPosition + 1));
      output[targetIndex + 1] |= lossy << (octet - overflow);
    }
  });
  return output;
}

async function retryRead() {
  var attempts = 5;
  var data = Buffer.alloc(0);
  do {
    try {
      await readResponse(true);
      data = await readResponse();
    } catch (e) {
      if (debug) {
        console.log("retryRead error", e);
      }
      await sleep(1000 / Math.max(attempts, 1));
    }
  } while (data.length === 0 && attempts-- > 0);
  if (attempts < 0) {
    throw new Error("Failed to get response");
  } else {
    return data.toString();
  }
}

async function sendCommand(cmd) {
  let command = Buffer.from(cmd);
  command = Buffer.from([PREFIX, ...command, SUFFIX]);

  return new Promise((resolve, reject) => {
    const stream = fs.createWriteStream(device);
    stream.on("error", (error) => {
      reject(error);
    });

    stream.on("ready", () => {
      stream.write(command, null, (error) => {
        if (error) {
          reject(error);
        } else {
          stream.close(resolve);
        }
      });
    });
  });
}

// Bleed: bleed off any data from previous commands
async function readResponse(bleed = false) {
  return new Promise((resolve, reject) => {
    const stream = fs.createReadStream(device);
    var buff = Buffer.alloc(0);

    stream.on("error", (error) => {
      return reject(error);
    });

    /*
    if (debug) {
      stream.on("ready", () => {
        if (bleed) {
          console.log("bleed ready");
        } else {
          console.log("read ready");
        }
      });
    }
    */

    stream.on("data", (chunk) => {
      console.log(
        "\t",
        bleed ? "bleed" : "read",
        "data:",
        chunk.toString('hex')
      );
      buff = Buffer.concat([buff, chunk]);
      const end = buff.indexOf(NUL);
      stream.close(); // This does not close the stream.
      stream.push(null);
      stream.read(0); // Pushing a null and reading leads to close.

      if (bleed && buff.slice(0, end).length > 0) {
        // Don't count as a bleed if we get data.  We're looking for an immediate stream end event
        return reject("bleed with data");
      }

      if (end === NOT_FOUND) {
        reject("End of data not found");
      } else {
        resolve(buff.slice(0, end));
      }
    });

    stream.on("end", () => {
      if (bleed) {
        //console.log("bleed end");
        return resolve();
      }
      //console.log("read end");
      const end = buff.indexOf(NUL);
      stream.close(); // This does not close the stream.
      stream.push(null);
      stream.read(0); // Pushing a null and reading leads to close.

      if (end === NOT_FOUND) {
        reject("End of data not found");
      } else {
        resolve(buff.slice(0, end));
      }
    });
  });
}

main().catch(console.log);
