require("dotenv").config();
const log = {
  send: require("debug")("Midas:send"),
  recv: require("debug")("Midas:recv"),
};

const { NFC } = require("nfc-pcsc");
const { SAM_READER } = process.env;

const CLA = 0x00;

const INS = {
  GET_RANDOM: 0x10,
  SET_DIVERSIFICATION_KEY: 0x20,
  GET_DIVERSIFIED_KEY: 0x21,
};

class MidasClient {
  constructor() {
    const nfc = new NFC();

    // I am cheating and using nfc-pcsc even though this is a smart card interface
    nfc.on("reader", async (reader) => {
      reader.autoProcessing = false;
      if (reader.reader.name === SAM_READER) {
        console.log("sam\t", reader.reader.name);
        this.reader = reader;
      }

      reader.on("error", (err) => {
        console.error(`an error occurred`, reader, err);
      });

      reader.on("end", () => {
        console.info(`device removed`);
        throw new Error("encoder(s) removed");
      });
    });

    nfc.on("error", (err) => {
      console.error(`an error occurred`, err);
    });
  }

  async getRandom() {
    const res = await this.send(
      [0x00, INS.GET_RANDOM, 0x00, 0x00, 0x00],
      "getRandom"
    );
    if (res.slice(-1)[0] !== 0x00) {
      throw new Error("error with getRandom");
    }
    return res.slice(0, -2);
  }

  async getDiversifiedKey(material) {
    const res = await this.send(
      [0x00, INS.GET_DIVERSIFIED_KEY, 0x00, 0x00, material.length, ...material],
      "getDiversifiedKey"
    );
    if (res.slice(-1)[0] !== 0x00) {
      throw new Error("error with getDiversifiedKey");
    }
    return res.slice(0, -2);
  }

  async send(cmd, comment = null, responseMaxLength = 40) {
    const { reader } = this;
    const b =
      typeof cmd === "string" ? Buffer.from(cmd, "hex") : Buffer.from(cmd);
    log.send((comment ? `[${comment}] ` : "") + `sending`, b);
    const data = await reader.transmit(b, responseMaxLength);
    log.recv((comment ? `[${comment}] ` : "") + `received data`, data);
    return data;
  }
}

module.exports = MidasClient;
