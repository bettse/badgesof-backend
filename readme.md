# Badgesof Backend

#### Service

1. `sudo cp badgesof.service /lib/systemd/system/`
2. `sudo chmod 644 /lib/systemd/system/badgesof.service`
3. `sudo systemctl daemon-reload`
4. `sudo systemctl enable badgesof.service`
5. `sudo systemctl start badgesof.service`

`journalctl --since today -f -u badgesof.service`
