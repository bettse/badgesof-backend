require("dotenv").config();
const crypto = require("crypto");
const NDEF = require("ndef");

const { NODE_ENV, NDEF_PASSWORD } = process.env;

const uidAsciiLength = 14;
const cmacAsciiLength = 16;
const counterAsciiLength = 6;

const NOT_FOUND = -1;
const CMAC_START_TAG = "{cmacStart}";
const CMAC_TAG = "{cmac}";
const UID_TAG = "{uid}";
const COUNTER_TAG = "{counter}";

const UID_REPLACEMENT = "U".repeat(uidAsciiLength);
const COUNTER_REPLACEMENT = "C".repeat(counterAsciiLength);
const CMAC_REPLACEMENT = "M".repeat(cmacAsciiLength);
const PWD_DISABLED = 0xff;

const PWD_AUTH = 0x1b;
const READ_SIG = 0x3c;

const pageLength = 4;
const startingPage = 4;

const ntagStructure = {
  0x12: {
    lastUserPage: 0x27,
    configPageNum: 0x29,
    pwdPageNum: 0x2b,
  },
  0x3e: {
    lastUserPage: 0x81,
    configPageNum: 0x83,
    pwdPageNum: 0x85,
  },
  0x6d: {
    lastUserPage: 0xe1,
    configPageNum: 0xe3,
    pwdPageNum: 0xe5,
  },
};

class NTAG21X {
  constructor(reader) {
    const { name } = reader;
    this.authMode = "acr122u";
    if (name.startsWith("ELYCTIS")) {
      //???
    } else if (name.startsWith("Identiv")) {
      this.authMode = "identiv";
    }

    this.reader = reader;
    this.type = "NTAG21X";
    this.config = [];
  }

  async getCardStructure() {
    const { reader } = this;
    if (this.structure) {
      return this.structure;
    }
    // Some readers only return 4 bytes, others 16
    this.CC = await reader.read(3, 16);
    const size = this.CC[2];
    this.structure = ntagStructure[size];
    return this.structure;
  }

  async getConfig() {
    const { reader } = this;
    const structure = await this.getCardStructure();
    const config = await reader.read(structure.configPageNum, 16);
    if (config.length === pageLength) {
      this.config[0] = config;
    } else {
      // Assume 16 bytes
      for (var i = 0; i < 4; i++) {
        this.config[i] = config.slice(
          pageLength * i,
          pageLength * i + pageLength
        );
      }
    }

    return this.config[0];
  }

  async encodeUrl(url, lock = false) {
    const { reader } = this;
    const { card } = reader;
    const { ndef, uidMirrorOffset, counterMirrorOffset } =
      this.generateNDEF(url);
    const records = await this.readNDEF();
    if (records && records.length > 0) {
      console.log({ records });
    }

    if (NODE_ENV === "development") {
      console.log("skipping writes", {
        url,
        lock,
        ndef: ndef.toString("hex"),
        uidMirrorOffset,
        counterMirrorOffset,
      });
      return;
    }
    try {
      await this.writeNdef(ndef);
      await this.writeMirrors({ uidMirrorOffset, counterMirrorOffset });
      if (lock) {
        await this.lock();
      }
      console.log("encoded", { url, lock });
    } catch (e) {
      console.log("Error encoding url", e);
    }
  }

  generateNDEF(url) {
    url = url.replace(UID_TAG, UID_REPLACEMENT);
    url = url.replace(COUNTER_TAG, COUNTER_REPLACEMENT);
    url = url.replace(CMAC_TAG, CMAC_REPLACEMENT);
    url = url.replace(CMAC_START_TAG, "");

    const message = [NDEF.uriRecord(url)];

    const bytes = Buffer.from(NDEF.encodeMessage(message));
    if (bytes.length >= 255) {
      throw new Error("No support for ndef > 255 yet");
    }

    // Maybe do repalcement/indexof here?
    const buffer = Buffer.alloc(3 + bytes.length);
    buffer[0] = 0x03; // NDEF TLV tag
    buffer[1] = bytes.length;
    bytes.copy(buffer, 2);
    buffer[buffer.length - 1] = 0xfe; // Terminator TLV tag

    let uidMirrorOffset = buffer.indexOf("U".repeat(uidAsciiLength));
    let counterMirrorOffset = buffer.indexOf("C".repeat(counterAsciiLength));
    if (uidMirrorOffset !== NOT_FOUND) {
      buffer.fill("0", uidMirrorOffset, uidMirrorOffset + uidAsciiLength);
    }
    if (counterMirrorOffset !== NOT_FOUND) {
      buffer.fill(
        "0",
        counterMirrorOffset,
        counterMirrorOffset + counterAsciiLength
      );
    }

    //NTAG 21x limit the uid/counter mirroring in a number of ways
    if (uidMirrorOffset !== NOT_FOUND && counterMirrorOffset !== NOT_FOUND) {
      if (uidMirrorOffset > counterMirrorOffset) {
        throw new Error(
          `Counter mirror must come after UID mirror (${uidMirrorOffset} > ${counterMirrorOffset})`
        );
      }
      if (counterMirrorOffset !== uidMirrorOffset + uidAsciiLength + 1) {
        throw new Error(
          `Counter mirror must be one character after UID mirror end`
        );
      }
    }

    const results = {
      ndef: buffer,
    };
    if (uidMirrorOffset !== NOT_FOUND) {
      results["uidMirrorOffset"] = uidMirrorOffset;
    }

    if (counterMirrorOffset !== NOT_FOUND) {
      results["counterMirrorOffset"] = counterMirrorOffset;
    }
    return results;
  }

  async getUid() {
    if (this.uid) {
      return this.uid;
    }
    const { reader } = this;
    const responseMaxLength = 12; // 10 byte uid + 2 byte status

    const res = await reader.transmit(
      Buffer.from([0xff, 0xca, 0x00, 0x00, 0x00]),
      responseMaxLength
    );
    if (res.slice(-1)[0] !== 0x00) {
      throw new Error("error getting uid");
    }
    this.uid = res.slice(0, -2);

    return this.uid;
  }

  async calculateKey() {
    if (this.key) {
      return this.key;
    }
    const uid = await this.getUid();
    this.key = crypto.pbkdf2Sync(
      NDEF_PASSWORD,
      uid.toString("hex"),
      1000,
      4,
      "sha512"
    );
    console.log("Diversified Key:", this.key.toString("hex"));
    return this.key;
  }

  async readNDEF() {
    const { reader } = this;
    const structure = await this.getCardStructure();
    const { lastUserPage } = structure;

    // The Evolis NFC encoder (ELYCTIS) returns 16 bytes
    const blockSize = 16;
    const TLV = {
      NULL: 0,
      LockControl: 1,
      MemoryControl: 2,
      NDEFMessage: 3,
      Terminator: 0xfe,
    };
    let pageNum = startingPage;
    let index = 0; // The offset within the page
    let ndefFound = false;
    do {
      const page = await reader.read(pageNum, blockSize);
      switch (page[index++]) {
        case TLV.LockControl:
          index += 1 + page[index];
          break;
        case TLV.NDEFMessage:
          ndefFound = true;
          break;
        default:
          console.log("Buffer didn't start with handled NDEF TLV");
          return Buffer.alloc(0);
      }
      pageNum += Math.floor(index / pageLength);
      index = index % pageLength;
    } while (!ndefFound && pageNum < lastUserPage);

    const page = await reader.read(pageNum, blockSize);
    const length = page[index];
    if (length === 0 && page[index + 1] === TLV.Terminator) {
      // Very likely a factory default tag
      return [];
    } else if (length === 0xff) {
      throw new Error("No support for ndef > 255 yet");
    }

    const byteCount = Math.ceil(length / blockSize) * blockSize;

    let buffer = Buffer.alloc(0);
    buffer = await reader.read(pageNum, byteCount);
    buffer = buffer.slice(index + 1, index + 1 + length);

    const records = NDEF.decodeMessage(buffer);
    return records
      .map((record) => {
        switch (record.tnf) {
          case NDEF.TNF_WELL_KNOWN:
            switch (record.type) {
              case NDEF.RTD_URI:
                return NDEF.uri.decodePayload(record.payload);
              default:
                console.log("other type", record.type);
            }
          default:
            console.log("other tnf", record.tnf);
        }
        return null;
      })
      .filter((x) => x);
  }

  async writeNdef(data) {
    const { reader } = this;
    const structure = await this.getCardStructure();
    const { lastUserPage } = structure;

    for (var page = startingPage; page <= lastUserPage; page++) {
      const start = (page - startingPage) * pageLength;
      const end = Math.min(start + pageLength, data.length);
      const pad = Buffer.alloc(pageLength - (end - start));
      const pageData = Buffer.concat([data.slice(start, end), pad]);
      if (pad.length < pageLength) {
        await reader.write(page, pageData);
      }
    }
  }

  async writeMirrors({ uidMirrorOffset, counterMirrorOffset }) {
    const { reader } = this;
    const structure = await this.getCardStructure();
    const configPage = await this.getConfig();

    const MIRROR = configPage[0];
    const MIRROR_PAGE = configPage[2];
    const MIRROR_CONF = (MIRROR & 0xc0) >> 6;
    const MIRROR_BYTE = (MIRROR & 0x30) >> 4;

    let new_mirror_conf = 0x00;
    if (uidMirrorOffset) {
      new_mirror_conf = new_mirror_conf | 0x01;
    }
    if (counterMirrorOffset) {
      new_mirror_conf = new_mirror_conf | 0x02;
    }

    const new_mirror_page =
      startingPage + Math.floor(uidMirrorOffset / pageLength);
    const new_mirror_byte = uidMirrorOffset % pageLength;
    const new_mirror =
      (MIRROR & 0x0f) | (new_mirror_conf << 6) | (new_mirror_byte << 4);
    const new_config = Buffer.from([
      new_mirror,
      configPage[1],
      new_mirror_page,
      configPage[3],
    ]);

    if (!new_config.equals(configPage)) {
      await reader.write(structure.configPageNum, new_config);
    }

    if (counterMirrorOffset) {
      // TODO: this.getConfig(pageNum);
      const config_1 = await reader.read(structure.configPageNum + 1);
      if ((config_1[0] & 0x10) === 0) {
        const new_config_1 = Buffer.from(config_1);
        config_1[0] = config_1[0] | 0x10;
        console.log("NFC counter not enabled, enabling");
        await reader.write(structure.configPageNum + 1, new_config_1);
      }
    }
  }

  async lock() {
    const { reader } = this;
    const structure = await this.getCardStructure();
    const key = await this.calculateKey();
    // Write PWD
    await reader.write(structure.pwdPageNum, key);

    // Write AUTH0 to lock card
    const config = await this.getConfig();
    const AUTH0 = config[3];
    if (AUTH0 === PWD_DISABLED) {
      config[3] = 0x00;
      await reader.write(structure.configPageNum, config);
    }
  }
}

module.exports = NTAG21X;
