require("dotenv").config();
const fs = require("fs");
const B2 = require("backblaze-b2");
const { InfluxDB, Point } = require("@influxdata/influxdb-client");
const Bot = require("keybase-bot");

const LaserPrinter = require("./LaserPrinter");
const EvolisPrimacy = require("./EvolisPrimacy");
const SocketHookClient = require("./SocketHookClient");
const MidasClient = require("./MidasClient");
const DB = require("./db");

const { NODE_ENV } = process.env;

const {
  KEYBASE_BOT_USERNAME,
  KEYBASE_BOT_PAPERKEY,
  KEYBASE_CHANNEL,
  KEYBASE_TEAM,
  KEYBASE_CONVERSATION_ID,
} = process.env;

const { B2_KEY_ID, B2_APPLICATION_KEY, B2_BUCKET_ID, B2_BUCKET_NAME } =
  process.env;

const { REQUEST_NOTIFICATION_URL, REQUEST_NOTIFICATION_SHA } = process.env;

const { TAG_UPDATE_SHA } = process.env;
const access_granted_sha1 = "3816753e461791380bc38197a9be7cef853ee8bd";

const system_id = Buffer.from("accessgranted");
const badge_prefix = "badges";
const BADGESOF_URL = "https://badgesof.ericbetts.dev/?uid={uid}";

const validVariants = [
  "primary",
  "secondary",
  "success",
  "danger",
  "warning",
  "info",
  "light",
  "dark",
];

const db = new DB();

const laserPrinter = new LaserPrinter();
const evolisPrimacy = new EvolisPrimacy();
const requestHookClient = new SocketHookClient(
  REQUEST_NOTIFICATION_SHA,
  newRequest
);
const updateHookClient = new SocketHookClient(TAG_UPDATE_SHA, tagUpdate);
const midas = new MidasClient();

// Lazy init keybase bot
let bot;

const b2 = new B2({
  applicationKeyId: B2_KEY_ID,
  applicationKey: B2_APPLICATION_KEY,
});

async function updatePrinterStatus() {
  const conn = await db.setup();
  const PrinterStatus = conn.model("PrinterStatus");
  const newStatus = await evolisPrimacy.getStatus();
  await PrinterStatus.findOneAndUpdate({ name: "EvolisPrimacy" }, newStatus, {
    upsert: true,
  }).exec();
}

const updateFrequencyHours = 24;
setInterval(() => {
  updatePrinterStatus();
}, 1000 * 60 * 60 * updateFrequencyHours);
updatePrinterStatus();

async function sleep(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
}

async function updateStatus(id, status, statusVariant = "info") {
  console.log("updateStatus", { id, status });
  if (NODE_ENV === "development") {
    return;
  }
  if (!validVariants.includes(statusVariant)) {
    console.log("Invalid statusVariant", statusVariant);
    statusVariant = "primary";
  }

  try {
    await SocketHookClient.send(`badgesOf/request/${id}`, {
      status,
      statusVariant,
    });
  } catch (e) {
    console.log("failed to update status over websocket", e);
  }

  try {
    const conn = await db.setup();
    const Request = conn.model("Request");
    await Request.findByIdAndUpdate(id, { status });
  } catch (e) {
    console.log("failed to update record status", e);
  }
}

async function downloadBadge(request) {
  const { badge } = request;
  const { contentType, sha1 } = badge;
  const parts = contentType.split("/");
  const ext = parts[parts.length - 1];

  try {
    await b2.authorize();

    const { data } = await b2.downloadFileByName({
      bucketName: B2_BUCKET_NAME,
      fileName: `${badge_prefix}/${sha1}.${ext}`,
      responseType: "arraybuffer",
    });
    const content = Buffer.from(data);
    return { content, ...badge };
  } catch (e) {
    console.log(e);
    throw new Error("Couldn't download file");
  }
}

async function tagUpdate(payload) {
  console.log(payload);
  const { id } = payload;
  const status = "posted";
  try {
    const conn = await db.setup();
    const Request = conn.model("Request");
    if (id) {
      const record = await Request.findByIdAndUpdate(id, { status });
      console.log("updated", record);
    }
  } catch (e) {
    console.log("error updating from uid", e);
  }
}

function isMaintenance() {
  return false;
  const now = new Date();
  const maintenance =
    (now.getUTCDay() === 6 && now.getUTCHours() >= 18) ||
    (now.getUTCDay() === 0 && now.getUTCHours() < 6);
  return maintenance; //  && NODE_ENV !== "development";
}

async function runWithStatus(request, status, callback) {
  const { id } = request;
  try {
    const result = await callback();
    await updateStatus(id, status);
    return result;
  } catch (e) {
    await updateStatus(id, `failure during ${status}`);
    throw e;
  }
}

async function newRequest({ id, printCard = true, printLetter = true }) {
  console.log("--------------------");
  await sleep(2000); // A little time for sockethook to get setup
  if (NODE_ENV !== "development" && isMaintenance()) {
    await updateStatus(id, "queued request during maintenance window");
    // Ignore requests, allow them to queue
    return;
  }
  let conn;
  try {
    conn = await db.setup();
  } catch (e) {
    console.log("error setting up db");
  }
  const Request = conn.model("Request");

  if (!id) {
    console.log("No id");
    return;
  }
  const request = await Request.findById(id).exec();
  if (!request) {
    console.log("No request found with id", id);
    return;
  }
  try {
    const { id, email, url = BADGESOF_URL } = request;
    const badge = await downloadBadge(request);

    // IDEA: maybe some sort of status wrapper?
    await updateStatus(id, "received");

    if (printLetter) {
      await laserPrinter.printAddress(request, badge);
      await updateStatus(id, "address printed");
    }

    if (printCard) {
      // Command to load badge
      const tag = await evolisPrimacy.loadCard();
      if (tag) {
        const uid = await tag.getUid();
        console.log(`${tag.type} with UID ${uid.toString("hex")}`);
        await updateStatus(id, `badge loaded (${uid.toString("hex")})`);
        await Request.findByIdAndUpdate(id, { uid: uid.toString("hex") });

        if (tag.type === "NTAG424") {
          console.log("TODO: something with NTAG424");
        } else if (tag.type === "NTAG21X") {
          // Command to encode badge
          await tag.encodeUrl(url);
        }

        await updateStatus(id, "badge encoded");
      } else {
        console.log("Plain PVC, no encoding");
      }

      if (badge.sha1 === access_granted_sha1) {
        console.log("Access granted badge, skipping");
        return;
      }
      await evolisPrimacy.printBadge(badge, email, url);
      await updateStatus(id, "badge printed", "success");

      await sendToKeybase(badge);
    }
  } catch (e) {
    console.log(e);
    updateStatus(id, "There was an error", "danger");
  }
}

async function sendToKeybase(badge) {
  if (NODE_ENV === "development") {
    return;
  }
  if (!bot) {
    bot = new Bot();
    await bot.init(KEYBASE_BOT_USERNAME, KEYBASE_BOT_PAPERKEY);
  }

  const { content, contentType, sha1 } = badge;
  const parts = contentType.split("/");
  const ext = parts[parts.length - 1];
  const filename = `${sha1}.${ext}`;

  fs.writeFileSync(filename, content);
  try {
    //await bot.chat.attach(KEYBASE_CONVERSATION_ID, filename);
    fs.rmSync(filename);
  } catch (e) {
    console.log("failed to send to keybase", e);
  }
}
