require("dotenv").config();

const sizeOf = require("image-size");
const sharp = require("sharp");
const fetch = require("node-fetch");

// portrait
const badgePx = {
  height: 1012,
  width: 638,
};

class Printer {
  async resizeImage(
    inputImage,
    width = badgePx.width,
    height = badgePx.height
  ) {
    const dimensions = sizeOf(inputImage);
    let image = null;
    if (dimensions.width < dimensions.height) {
      image = await sharp(inputImage)
        .resize(width, height, { fit: "inside" })
        .png()
        .toBuffer();
    } else {
      image = await sharp(inputImage)
        .resize(height, width, { fit: "inside" })
        .png()
        .toBuffer();
    }
    return {
      image,
      type: "image/png",
    };
  }
}

module.exports = Printer;
