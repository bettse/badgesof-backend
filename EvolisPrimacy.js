require("dotenv").config();

const util = require("util");
const fs = require("fs");
const ipp = require("ipp");
const sizeOf = require("image-size");
const fetch = require("node-fetch");
const { spawn, execSync } = require("child_process");
const { NFC } = require("nfc-pcsc");
const debug = require("debug")("EvolisPrimacy");
const { Encoder } = require("qr");
const PDFDocument = require("pdfkit");
const QRCode = require("qrcode");
const SVGtoPDF = require("svg-to-pdfkit");

const Printer = require("./Printer");
const NTAG424 = require("./NTAG424");
const NTAG21X = require("./NTAG21X");

const { NODE_ENV, BADGE_PRINTER_URI } = process.env;
const { CONTACT_READER, NFC_READER, SAM_READER } = process.env;
const { arch } = process;

const overlay = {
  /** Typical mag-stripe **/
  magstrip: [
    [0, 0, 439, 1016],
    [588, 0, 648, 1016],
  ],
};

const badgePt = {
  height: 155.51,
  width: 243.84,
};

const DECIMAL = 10;
const PREFIX = 0x1b; //ESCAPE
const SEPARATOR = ";";
const SUFFIX = 0x0d; // CARRIAGE RETURN
const NOT_FOUND = -1;
const NUL = 0x00;
const precision = 256;

const nativeCommands = false;

const COMM_ERROR =
  "ERROR: evo_write_usb_port - write error (sync)\nERROR: fail to write\n";

const PCSCAID = Buffer.from("A000000306", "hex");

// To allow me to initialize the readers list before they have been detected
class FakeReader {
  constructor(name) {
    this.reader = {
      name,
    };
    this.ribbon = "";
  }
  async read(blockNumber, length, blockSize = 4, packetSize = 16) {
    console.log("read", this.reader.name, {
      blockNumber,
      length,
      blockSize,
      packetSize,
    });
  }

  async write(page, data) {
    console.log("write", this.reader.name, page, data);
  }
}

// TODO: allow this as a parameter to support instances of EvolisPrimacy for multiple printers
class EvolisPrimacy extends Printer {
  constructor(uri = BADGE_PRINTER_URI, dev = "/dev/evolis") {
    super();
    const printer = ipp.Printer(uri);
    this.print = util.promisify(printer.execute.bind(printer));

    const nfc = new NFC();
    this.device = execSync(`readlink -f ${dev}`).toString().replace("\n", "");

    this.readers = {
      nfc: new FakeReader("nfc"),
      sam: new FakeReader("sam"),
      contact: new FakeReader("contact"),
    };
    nfc.on("reader", async (reader) => {
      reader.autoProcessing = false; // In order to handle both type 2 and type 4
      if (reader.reader.name.startsWith(NFC_READER)) {
        console.log("nfc\t", reader.reader.name);
        this.readers.nfc = reader;
      } else if (reader.reader.name.startsWith(CONTACT_READER)) {
        console.log("contact\t", reader.reader.name);
        this.readers.contact = reader;
      } else if (reader.reader.name.startsWith(SAM_READER)) {
        console.log("sam\t", reader.reader.name);
        this.readers.sam = reader;
      } else {
        console.log("unknown", reader.reader.name);
      }

      reader.on("card.off", (card) => {
        //A bit chatty
        //console.log(`${reader.reader.name} card removed`, card);
      });

      reader.on("error", (err) => {
        console.error(`an error occurred`, reader, err);
      });

      reader.on("end", () => {
        console.info(`device removed`);
        throw new Error("encoder(s) removed");
      });
    });

    nfc.on("error", (err) => {
      console.error(`an error occurred`, err);
    });
  }

  async getRemaining() {
    const result = await this.sendCommand("Rrt;count");
    return parseInt(result, DECIMAL);
  }

  async getStatus() {
    const commands = [];
    commands.push("Rr");
    const keywords = ["zone", "label", "ref", "date", "qty", "count"];
    keywords.forEach((keyword) => {
      commands.push(`Rrt;${keyword}`);
    });
    commands.push("Rlr;e");
    commands.push("Rlr;p");
    commands.push("Rlr;f");
    commands.push("Rlr;c");
    commands.push("Rlr;h");
    commands.push("Rlr;b");
    let result = await this.sendCommand(commands);
    if (result.endsWith('> ')){
      result = result.slice(0, -2);
    }

    const parts = result
      .split(">")
      .map((part) => part.replace("\n", "").trim());
    if (parts.length !== commands.length) {
      throw new Error("Unequal number of responses to commands");
    }
    //console.log(parts);
    const tag = {
      zone: parts[1],
      label: parts[2],
      ref: parts[3],
      date: new Date(parseInt(parts[4], DECIMAL) * 1000),
      qty: parseInt(parts[5] || '0', DECIMAL),
      count: parseInt(parts[6] || '0', DECIMAL),
    };
    this.ribbon = tag.label;
    const status = {
      ribbon_finished: parts[7],
      card_position: parts[8],
      feeder: parts[9],
      cleaning: parts[10],
      hopper: parts[11],
      bezel: parts[12],
    };
    return { ribbon: parts[0], tag, status };
  }

  async printBadge(badge, email, url = "") {
    try {
      const pdf = await this.composePdf(badge, url);

      const badgeRequest = {
        "operation-attributes-tag": {
          "requesting-user-name": email,
          "document-format": "application/pdf",
        },
        "job-attributes-tag": {
          sides: "two-sided-long-edge",
        },
        data: pdf,
      };

      if (NODE_ENV === "development") {
        console.log("badgeRequest", badgeRequest);
        console.log("eject loaded card");
        await this.sendCommand("Sie");
        return;
      }

      const badgeResult = await this.print("Print-Job", badgeRequest);
      // console.log("badgeResult", await badgeResult);
    } catch (e) {
      console.error("Evolis Print-Job error", e);
    }
  }

  async createQR(value) {
    return QRCode.toString(value, {
      errorCorrectionLevel: "Q",
      margin: 0,
      scale: 1,
      type: "svg",
    });
  }

  async composePdf(badge, url) {
    const { content } = badge;

    const colorCallback = ([[r, g, b], a]) => {
      // console.log("colorCallback", { r, g, b, a });
      if (a == 0) {
        return [[0, 0, 0, 0], 1];
      } else if (r === 0 && b === 0 && g === 0) {
        return [[0, 0, 0, 255], 1];
      } else {
        return [[0, 0, 0, 0], 1];
      }
    };
    // 144pt = 2in
    const QR = {
      x: (badgePt.width - 144) / 2,
      y: (badgePt.height - 144) / 2,
      options: {
        assumePt: true,
        height: 144,
        width: 144,
        colorCallback,
      },
    };

    try {
      const { image, type } = await this.resizeImage(content);
      const QRsvg = await this.createQR(url);

      return new Promise((resolve, reject) => {
        const doc = new PDFDocument({
          layout: "landscape",
          size: [badgePt.height, badgePt.width],
        });

        let buffers = [];
        doc.on("error", reject); //dunno if it actually emits this
        doc.on("data", buffers.push.bind(buffers));
        doc.on("end", () => resolve(Buffer.concat(buffers)));

        if (NODE_ENV === "development") {
          console.log({ NODE_ENV }, "Piping to badge.pdf");
          doc.pipe(fs.createWriteStream("./badge.pdf")); // write to PDF
        }

        doc.save();
        const dimensions = sizeOf(image);
        if (dimensions.width < dimensions.height) {
          doc.rotate(90);
          doc.image(image, 0, -badgePt.width, {
            fit: [badgePt.height, badgePt.width],
          });
        } else {
          doc.image(image, 0, 0, { fit: [badgePt.width, badgePt.height] });
        }
        doc.restore();

        /* PAGE 2: QR Code */
        if (this.ribbon === "Color YMCKOK") {
          doc.addPage({
            layout: "landscape",
            size: [badgePt.height, badgePt.width],
          });

          SVGtoPDF(doc, QRsvg, QR.x, QR.y, QR.options);
        }
        doc.end();
      });
    } catch (e) {
      console.error("compose PDF error", e);
    }
  }

  // Mechanical/evocom
  async loadCard() {
    await this.sendCommand("Sic");

    const reader = this.readers.nfc;
    const { card } = reader;
    if (!card) {
      return false;
    }

    if (isNtag21x(card)) {
      return new NTAG21X(this.readers.nfc);
    } else if (card.standard === "TAG_ISO_14443_4") {
      return new NTAG424(this.readers.nfc);
    }
    return false;
  }

  evoBin(arch) {
    if (arch === "arm") {
      return "./evocomRaspberryPiOS";
    } else if (arch === "x64") {
      return "./evocomYocto";
    } else {
      throw new Error("Unsupported arch", arch);
    }
  }

  // Accept command (string or array)
  async sendCommand(cmd) {
    if (nativeCommands) {
      return this.sendCommandNative(cmd);
    } else {
      return this.sendCommandEvocom(cmd);
    }
  }

  async sendCommandNative(cmd) {
    const cmds = [cmd].flat(); // convert both a String or Array into an Array
    const results = [];

    for (var i = 0; i < cmds.length; i++) {
      debug("send", cmds[i]);
      await this.writeCommand(cmds[i]);
      results[i] = await this.retryRead();
      debug("receive", results[i]);
    }
    const output = results.join("\n>");
    return output;
  }

  async sendCommandEvocom(cmd) {
    const { device } = this;
    const bin = this.evoBin(arch);
    cmd = [cmd].flat(); // convert both a String or Array into an Array
    if (cmd.length > 1) {
      fs.writeFileSync("commands.txt", cmd.join("\n"));
      cmd = ["-f", "commands.txt"];
    }
    const parameters = ["-d", device, ...cmd];
    console.log("sendCommand", bin, parameters);
    const process = spawn(bin, parameters);
    return new Promise((resolve, reject) => {
      const output = [];

      process.stdout.on("data", (data) => {
        output.push(data.toString());
      });

      process.stderr.on("data", (data) => {
        output.push(data.toString());
      });

      process.on("error", (error) => {
        reject(error);
      });

      process.on("close", (code) => {
        resolve(output.join("").replace("\n", ""));
      });
    });
  }

  async retryRead() {
    var attempts = 5;
    var data = Buffer.alloc(0);
    do {
      try {
        await this.readResponse(true);
        data = await this.readResponse();
      } catch (e) {
        debug("retryRead error", e);
        await sleep(1000 / Math.max(attempts, 1));
      }
    } while (data.length === 0 && attempts-- > 0);
    if (attempts < 0) {
      throw new Error("Failed to get response");
    } else {
      return data.toString();
    }
  }

  async writeCommand(cmd) {
    const { device } = this;
    let command = Buffer.from(cmd);
    command = Buffer.from([PREFIX, ...command, SUFFIX]);

    return new Promise((resolve, reject) => {
      const stream = fs.createWriteStream(device);
      stream.on("error", (error) => {
        reject(error);
      });

      stream.on("ready", () => {
        stream.write(command, null, (error) => {
          if (error) {
            reject(error);
          } else {
            stream.close(resolve);
          }
        });
      });
    });
  }

  // Bleed: bleed off any data from previous commands
  async readResponse(bleed = false) {
    const { device } = this;
    return new Promise((resolve, reject) => {
      const stream = fs.createReadStream(device);
      var buff = Buffer.alloc(0);

      stream.on("error", (error) => {
        return reject(error);
      });

      stream.on("ready", () => {
        if (bleed) {
          debug("bleed ready");
        } else {
          debug("read ready");
        }
      });

      stream.on("data", (chunk) => {
        //console.log("\tdata: ", chunk.toString());
        if (bleed) {
          stream.close(); // This does not close the stream.
          stream.push(null);
          stream.read(0); // Pushing a null and reading leads to close.
          // Don't count as a bleed if we get data.  We're looking for an immediate stream end event
          return reject("bleed with data");
        }
        buff = Buffer.concat([buff, chunk]);

        const end = buff.indexOf(NUL);
        stream.close(); // This does not close the stream.
        stream.push(null);
        stream.read(0); // Pushing a null and reading leads to close.

        if (end === NOT_FOUND) {
          reject("End of data not found");
        } else {
          resolve(buff.slice(0, end));
        }
      });

      stream.on("end", () => {
        if (bleed) {
          //console.log("bleed end");
          return resolve();
        }
        //console.log("read end");
        const end = buff.indexOf(NUL);
        stream.close(); // This does not close the stream.
        stream.push(null);
        stream.read(0); // Pushing a null and reading leads to close.

        if (end === NOT_FOUND) {
          reject("End of data not found");
        } else {
          resolve(buff.slice(0, end));
        }
      });
    });
  }
}

function isNtag21x(card) {
  let ntag = false;
  if (card.standard !== "TAG_ISO_14443_3") {
    return false;
  }

  //https://flomio.com/forums/topic/list-of-apdu-codes/#post-20335
  const { atr } = card;
  const rid = atr.slice(7, 7 + 5);
  if (!rid.equals(PCSCAID)) {
    return false;
  }

  const cardName = atr.readUint16BE(13);
  switch (cardName) {
    case 0x0001: //Mifare 1k
      console.log("Mifare 1k");
      break;
    case 0x0002: //Mifare 4k
      console.log("Mifare 3k");
      break;
    case 0x0003: //Mifare Ultralight
      console.log("Mifare Ultralight (NTAG)");
      ntag21x = true;
      break;
    case 0x0026: //Mifare Mini
      console.log("Mifare Mini");
      break;
    case 0xf004: //Topaz and Jewel
      console.log("Topaz/Jewel");
      break;
    case 0xf011: // Felica 212k
      console.log("Felica 212k");
      break;
    case 0xf012: // Felica 424k
      console.log("Felica 424k");
    default:
      console.log("unknown cardName", cardName);
      break;
  }
  return ntag21x;
}

async function sleep(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
}

module.exports = EvolisPrimacy;
